/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.model;

import com.wpsj.da.ProductDataAccess;
import com.wpsj.entity.Product;
import java.util.List;

/**
 *
 * @author nam oio
 */
public class ProductFinderBean {
    private String keyword;
    public void setKeyword(String keyword){
        this.keyword=keyword;
        
    }
    public List<Product> getProduct(){
      
      return new ProductDataAccess().getProductByName(keyword);
    }
    
     public boolean edit(int id,String name,String desc){
        
      return new ProductDataAccess().edit1(id, name, desc);
    }
      public boolean delete(int id){
        
      return new ProductDataAccess().delete1(id);
    }
     
     public boolean login(String name,String pass){
        
      return new ProductDataAccess().login(name,pass);
    }
        public List<Product> getAll(){
      
      return new ProductDataAccess().getProduct();
    }

  

    
}
