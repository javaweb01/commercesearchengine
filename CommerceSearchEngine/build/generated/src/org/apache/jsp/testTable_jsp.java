package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class testTable_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_out_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_if_test;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_out_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_c_out_value_nobody.release();
    _jspx_tagPool_c_if_test.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("  ");
      com.wpsj.model.ProductFinderBean finder1 = null;
      synchronized (request) {
        finder1 = (com.wpsj.model.ProductFinderBean) _jspx_page_context.getAttribute("finder1", PageContext.REQUEST_SCOPE);
        if (finder1 == null){
          finder1 = new com.wpsj.model.ProductFinderBean();
          _jspx_page_context.setAttribute("finder1", finder1, PageContext.REQUEST_SCOPE);
        }
      }
      out.write("\n");
      out.write("  ");
      if (_jspx_meth_c_if_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <table id=\"table\" class=\"table data\">\n");
      out.write("            <thead>\n");
      out.write("                <tr>\n");
      out.write("                    <th>Id</th>\n");
      out.write("                    <th>Name</th>\n");
      out.write("                    <th>Desc</th>\n");
      out.write("                    <th>Actions <button class=\"add\">Add new</button></th>\n");
      out.write("                </tr>\n");
      out.write("            </thead>\n");
      out.write("            <tbody>\n");
      out.write("                ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("            </tbody>\n");
      out.write("        </table>\n");
      out.write("    </body>\n");
      out.write("\n");
      out.write("\n");
      out.write("<style>\n");
      out.write("    body {\n");
      out.write("        font-family: Arial, sans-serif;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .table {\n");
      out.write("        width: 100%;\n");
      out.write("    }\n");
      out.write("    .table thead th {\n");
      out.write("        padding: 10px 10px;\n");
      out.write("        background: #00adee;\n");
      out.write("        font-size: 15px;\n");
      out.write("        text-transform: uppercase;\n");
      out.write("        vertical-align: top;\n");
      out.write("        color: #1D4A5A;\n");
      out.write("        font-weight: normal;\n");
      out.write("        text-align: left;\n");
      out.write("    }\n");
      out.write("    .table tbody tr td {\n");
      out.write("        padding: 10px;\n");
      out.write("        background: #f2f2f2;\n");
      out.write("        font-size: 14px;\n");
      out.write("        line-height: 1.8rem;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .add, .edit, .save, .delete ,.cancel{\n");
      out.write("        outline: none;\n");
      out.write("        background: none;\n");
      out.write("        border: none;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .edit, .save, .delete ,.cancel{\n");
      out.write("        padding: 5px 10px;\n");
      out.write("        cursor: pointer;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .add {\n");
      out.write("        float: right;\n");
      out.write("        background: transparent;\n");
      out.write("        border: 1px solid #ffffff;\n");
      out.write("        color: #ffffff;\n");
      out.write("        font-size: 13px;\n");
      out.write("        padding: 0;\n");
      out.write("        padding: 3px 5px;\n");
      out.write("        cursor: pointer;\n");
      out.write("    }\n");
      out.write("    .add:hover {\n");
      out.write("        background: #ffffff;\n");
      out.write("        color: #00adee;\n");
      out.write("    }\n");
      out.write("    .loading{\n");
      out.write("         display: none;\n");
      out.write("    }\n");
      out.write("    .save {\n");
      out.write("        display: none;\n");
      out.write("        background: #32AD60;\n");
      out.write("        color: #ffffff;\n");
      out.write("    }\n");
      out.write("    .save:hover {\n");
      out.write("        background: #27854a;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .edit {\n");
      out.write("        background: #2199e8;\n");
      out.write("        color: #ffffff;\n");
      out.write("    }\n");
      out.write("    .edit:hover {\n");
      out.write("        background: #147dc2;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .delete ,.cancel{\n");
      out.write("        background: #EC5840;\n");
      out.write("        color: #ffffff;\n");
      out.write("    }\n");
      out.write("    .delete:hover ,.cancel{\n");
      out.write("        background: #e23317;\n");
      out.write("        \n");
      out.write("    }\n");
      out.write("    .cancel{\n");
      out.write("         display: none;\n");
      out.write("    }\n");
      out.write("body input[type=\"text\"] {\n");
      out.write("  padding-left: 0.5rem;\n");
      out.write("  margin-left: -0.5rem;\n");
      out.write("  width: 100%;\n");
      out.write("  line-height: 1.8rem;\n");
      out.write("  font-size: 1rem;\n");
      out.write("  font-family: inherit;\n");
      out.write("  background-color: #f5f5f5;\n");
      out.write("  border: 0px;\n");
      out.write("  \n");
      out.write("}\n");
      out.write("\n");
      out.write("</style>\n");
      out.write("<script src=\"jquery-3.3.1.js\" type=\"text/javascript\"></script>\n");
      out.write("<script src=\"ajax.js\" type=\"text/javascript\"></script>\n");
      out.write("<script src=\"jquery-3.3.1.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<script>\n");
      out.write("    var v,b,c;\n");
      out.write("    var a=0;\n");
      out.write("    $(document).on('click', '.edit', function () {\n");
      out.write("        $(this).parent().siblings('td.data').each(function () {\n");
      out.write("            var content = $(this).html();\n");
      out.write("            a++;\n");
      out.write("            if(a==1){\n");
      out.write("                v=content;\n");
      out.write("            }\n");
      out.write("            if(a==2){\n");
      out.write("                  b=content;\n");
      out.write("            }\n");
      out.write("            if(a==3){\n");
      out.write("                  c=content;\n");
      out.write("            }\n");
      out.write("            $(this).html('<input type=\"text\" value=\"' + content + '\" />');\n");
      out.write("        });\n");
      out.write("\n");
      out.write("        $(this).siblings('.save').show();\n");
      out.write("        $(this).siblings('.cancel').show();\n");
      out.write("        $(this).siblings('.delete').hide();\n");
      out.write("        $(this).hide();\n");
      out.write("    });\n");
      out.write("\n");
      out.write("    $(document).on('click', '.save', function () {\n");
      out.write("\n");
      out.write("var id=\"\";\n");
      out.write("var name=\"\";\n");
      out.write("var desc=\"\";\n");
      out.write("var dem=0;\n");
      out.write("  $('input').each(function () {\n");
      out.write("      dem++;\n");
      out.write("       var content = $(this).val();\n");
      out.write("          \n");
      out.write("      if(dem==1){\n");
      out.write("         \n");
      out.write("           id=content;\n");
      out.write("      }if(dem==2){\n");
      out.write("           name=content;\n");
      out.write("      }\n");
      out.write("      if(dem==3){\n");
      out.write("           desc=content;\n");
      out.write("      }\n");
      out.write("           \n");
      out.write("        });\n");
      out.write("\n");
      out.write("      \n");
      out.write("  $.ajax({\n");
      out.write("       \n");
      out.write("            type: \"GET\",\n");
      out.write("            data: {name: name,id: id, desc: desc},\n");
      out.write("            url: \"Edit\",\n");
      out.write("            success: function (responseJson) {\n");
      out.write("                $.each(responseJson, function (key, value) {\n");
      out.write("                  \n");
      out.write("if(value==\"true\"){\n");
      out.write("            $('input').each(function () {\n");
      out.write("            var content = $(this).val();\n");
      out.write("            $(this).html(content);\n");
      out.write("            $(this).contents().unwrap();\n");
      out.write("        });\n");
      out.write("       $('.save').hide();\n");
      out.write("              $('.edit').show();\n");
      out.write("              $('.cancel').hide();\n");
      out.write("                $('.delete').show();\n");
      out.write("}else{\n");
      out.write("      alert(\"lỗi\");\n");
      out.write("       \n");
      out.write("         \n");
      out.write("     \n");
      out.write("}\n");
      out.write("                });\n");
      out.write("            }\n");
      out.write("        })\n");
      out.write("\n");
      out.write("  \n");
      out.write("\n");
      out.write("    });\n");
      out.write("$(document).on('click', '.cancel', function () {\n");
      out.write("    $('.save').hide();\n");
      out.write("              $('.edit').show();\n");
      out.write("              $('.cancel').hide();\n");
      out.write("                $('.delete').show();\n");
      out.write("        var d=0;\n");
      out.write("      $('input').each(function () {\n");
      out.write("            var content = $(this).val();\n");
      out.write("            d++;\n");
      out.write("            if(d==1){\n");
      out.write("                $(this).html(v);\n");
      out.write("            $(this).contents().unwrap();\n");
      out.write("                \n");
      out.write("            }\n");
      out.write("            \n");
      out.write("             if(d==2){\n");
      out.write("                $(this).html(b);\n");
      out.write("            $(this).contents().unwrap();\n");
      out.write("                \n");
      out.write("            }\n");
      out.write("            \n");
      out.write("             if(d==3){\n");
      out.write("                $(this).html(c);\n");
      out.write("            $(this).contents().unwrap();\n");
      out.write("                \n");
      out.write("            }\n");
      out.write("            \n");
      out.write("            \n");
      out.write("        });\n");
      out.write("    });\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write(" $(function(){\n");
      out.write("        $('#table .delete').click(function(e) {\n");
      out.write("            e.preventDefault();\n");
      out.write("            var a=$(this).closest('tr').find('td:first').text();\n");
      out.write("           \n");
      out.write("              $.ajax({\n");
      out.write("            type: \"GET\",\n");
      out.write("            data: {name: \"delete\",id: a},\n");
      out.write("            url: \"Edit\",\n");
      out.write("            success: function (responseJson) {\n");
      out.write("                $.each(responseJson, function (key, value) {\n");
      out.write("                  \n");
      out.write("if(value==\"true\"){\n");
      out.write("          $(this).parents('tr').remove();\n");
      out.write("}else{\n");
      out.write("      alert(\"lỗi\");\n");
      out.write("       \n");
      out.write("         \n");
      out.write("     \n");
      out.write("}\n");
      out.write("                });\n");
      out.write("            }\n");
      out.write("        })\n");
      out.write("        });\n");
      out.write("    });\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    $('.add').click(function () {\n");
      out.write("        $(this).parents('table').append('<tr><td class=\"data\"></td><td class=\"data\"></td><td class=\"data\"></td><td><button class=\"save\">Save</button><button class=\"edit\">Edit</button> <button class=\"delete\">Delete</button></td></tr>');\n");
      out.write("    });\n");
      out.write("</script>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_if_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_0.setPageContext(_jspx_page_context);
    _jspx_th_c_if_0.setParent(null);
    _jspx_th_c_if_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${username==null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_0 = _jspx_th_c_if_0.doStartTag();
    if (_jspx_eval_c_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("       ");
        if (true) {
          _jspx_page_context.forward("login.jsp");
          return true;
        }
        out.write("\n");
        out.write("  ");
        int evalDoAfterBody = _jspx_th_c_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
    return false;
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${finder1.all}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("product");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                <tr>\n");
          out.write("                  \n");
          out.write("                    \n");
          out.write("                    <td class=\"data\">");
          if (_jspx_meth_c_out_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("</td>\n");
          out.write("                    <td class=\"data\">");
          if (_jspx_meth_c_out_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("</td>\n");
          out.write("                    <td class=\"data\">");
          if (_jspx_meth_c_out_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("</td>\n");
          out.write("                    <td>\n");
          out.write("                        <button class=\"save\">Save</button>\n");
          out.write("                        <button class=\"cancel\">Cancel</button>\n");
          out.write("                        <button class=\"edit\">Edit</button>\n");
          out.write("                        <button class=\"delete\">Delete</button>\n");
          out.write("                    </td>\n");
          out.write("                </tr>\n");
          out.write("         \n");
          out.write("                  ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_out_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_0 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_out_0.setPageContext(_jspx_page_context);
    _jspx_th_c_out_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_out_0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.id}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_out_0 = _jspx_th_c_out_0.doStartTag();
    if (_jspx_th_c_out_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_0);
      return true;
    }
    _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_0);
    return false;
  }

  private boolean _jspx_meth_c_out_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_1 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_out_1.setPageContext(_jspx_page_context);
    _jspx_th_c_out_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_out_1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.name}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_out_1 = _jspx_th_c_out_1.doStartTag();
    if (_jspx_th_c_out_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_1);
      return true;
    }
    _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_1);
    return false;
  }

  private boolean _jspx_meth_c_out_2(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_2 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_out_2.setPageContext(_jspx_page_context);
    _jspx_th_c_out_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_out_2.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.desc}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_out_2 = _jspx_th_c_out_2.doStartTag();
    if (_jspx_th_c_out_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_2);
      return true;
    }
    _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_2);
    return false;
  }
}
