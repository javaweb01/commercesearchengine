<%-- 
    Document   : login
    Created on : Mar 29, 2018, 4:19:01 PM
    Author     : nam oio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <form action="Login">
        <h2>Login</h2>
        <i class="fa fa-fw fa-user" aria-hidden="true"></i>
        <input type="text"
               value="${username}"
               name="username"
               class="user"
               placeholder="Username"/>

        <i class="fa fa-fw fa-lock" aria-hidden="true"></i>
        <input type="password" value="${password}"
               name="password"
               class="pass"
               placeholder="Password"/>

        <button class="login">LOGIN</button>
       
      </form>
    </body>
</html>
