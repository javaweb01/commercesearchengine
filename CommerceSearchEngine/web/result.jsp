<%-- 
    Document   : result
    Created on : Mar 29, 2018, 2:57:25 PM
    Author     : nam oio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
 <c:if test="${username==null}">
       <jsp:forward page="login.jsp"/>
  </c:if>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List Product</title>
    </head>
    <h1>List Product</h1>
    <a href="search.jsp">Search</a>
    <body>
        <jsp:useBean class="com.wpsj.model.ProductFinderBean" id="find" scope="request"/>
        <table>
            <tr>
                <td>Id</td>
                  <td>Name</td>
                    <td>Description</td>
                    
            </tr>
            <c:forEach items="${find.product}" var="product">
                <tr>
                    <td><c:out value="${product.id}"/></td>
                     <td><c:out value="${product.name}"/></td>
                      <td><c:out value="${product.desc}"/></td>
                </tr>
            </c:forEach>
        </table>
       
    </body>
</html>
