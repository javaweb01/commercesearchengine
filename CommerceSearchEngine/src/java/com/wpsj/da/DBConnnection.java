    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nam oio
 */
public class DBConnnection {
    private static Connection connection;
     public Connection openConnection() {
       
        try {
            
            
            try {
                try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                } catch (InstantiationException ex) {
                    Logger.getLogger(DBConnnection.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(DBConnnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DBConnnection.class.getName()).log(Level.SEVERE, null, ex);
            }

            String url = "jdbc:sqlserver://localhost:1433;databaseName=EmployeesManager";
            String username = "sa";
            String password = "123456";
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            Logger.getLogger(DBConnnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }
}
