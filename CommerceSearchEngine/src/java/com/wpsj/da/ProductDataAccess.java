/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import com.wpsj.entity.Product;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author nam oio
 */
public class ProductDataAccess {
    private PreparedStatement statement;
    private PreparedStatement getSearchStament() throws ClassNotFoundException,SQLException{
        if(statement==null){
            Connection connection=new DBConnnection().openConnection();
            statement=connection.prepareStatement("SELECT pro_id,pro_name,pro_desc FROM ProductStore WHERE pro_name like ?");
            
        }
        return statement;
    }
    
      private PreparedStatement getAll() throws ClassNotFoundException,SQLException{
        if(statement==null){
            Connection connection=new DBConnnection().openConnection();
            statement=connection.prepareStatement("SELECT pro_id,pro_name,pro_desc FROM ProductStore ");
            
        }
        return statement;
    }

       private PreparedStatement login1() throws ClassNotFoundException,SQLException{
        if(statement==null){
            Connection connection=new DBConnnection().openConnection();
            statement=connection.prepareStatement("  SELECT username,userpassword FROM users WHERE username=? and userpassword=? ");
            
        }
        return statement;
    }
      
        public boolean login(String name,String pass){
            boolean kt=false;
        try{
            PreparedStatement statement=login1();
            statement.setString(1,name);
             statement.setString(2,pass);
            ResultSet rs=statement.executeQuery();
            List<Product> products=new LinkedList<Product>();
            while(rs.next()){
              
               
               kt=true;
            }
            return kt;
        }catch(Exception ex){
            ex.printStackTrace();
            return kt;
        }
       
    }
       
        private PreparedStatement edit() throws ClassNotFoundException,SQLException{
        if(statement==null){
            Connection connection=new DBConnnection().openConnection();
            statement=connection.prepareStatement("UPDATE ProductStore SET pro_name=?,pro_desc=?  WHERE pro_id = ?");
           
        }
        return statement;
    }
    public ProductDataAccess() {
    }
    public boolean edit1(int id,String name,String desc){
        try{
            PreparedStatement statement=edit();
            statement.setString(1,name);
             statement.setString(2,desc);
              statement.setInt(3,id);
          
         statement.executeUpdate();
            return true;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
       
    }
    
    
         private PreparedStatement delete() throws ClassNotFoundException,SQLException{
        if(statement==null){
            Connection connection=new DBConnnection().openConnection();
            statement=connection.prepareStatement("DELETE FROM ProductStore WHERE pro_id = ?");
          
        }
        return statement;
    }
         
          public boolean delete1(int id){
        try{
            PreparedStatement statement=delete();
            statement.setInt(1,id);
            
          
         statement.executeUpdate();
            return true;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
       
    }
    public List<Product>  getProductByName(String name){
        try{
            PreparedStatement statement=getSearchStament();
            statement.setString(1,"%"+name+"%");
            ResultSet rs=statement.executeQuery();
            List<Product> products=new LinkedList<Product>();
            while(rs.next()){
              
               
                products.add(new Product(rs.getInt("pro_id"), rs.getString("pro_id"), rs.getString("pro_desc")));
            }
            return products;
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
       
    }
     public List<Product>  getProduct(){
        try{
            PreparedStatement statement=getAll();
            ResultSet rs=statement.executeQuery();
            List<Product> products=new LinkedList<Product>();
            while(rs.next()){
              
               
                products.add(new Product(rs.getInt("pro_id"), rs.getString("pro_name"), rs.getString("pro_desc")));
            }
            return products;
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
       
    }
}
