<%-- 
    Document   : testTable
    Created on : Mar 7, 2018, 4:43:52 PM
    Author     : nam oio
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <jsp:useBean class="com.wpsj.model.ProductFinderBean" id="finder1" scope="request"/>
  <c:if test="${username==null}">
       <jsp:forward page="login.jsp"/>
  </c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table id="table" class="table data">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Desc</th>
                    <th>Actions <button class="add">Add new</button></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${finder1.all}" var="product">
                <tr>
                  
                    
                    <td class="data"><c:out value="${product.id}"/></td>
                    <td class="data"><c:out value="${product.name}"/></td>
                    <td class="data"><c:out value="${product.desc}"/></td>
                    <td>
                        <button class="save">Save</button>
                        <button class="cancel">Cancel</button>
                        <button class="edit">Edit</button>
                        <button class="delete">Delete</button>
                    </td>
                </tr>
         
                  </c:forEach>
            </tbody>
        </table>
    </body>


<style>
    body {
        font-family: Arial, sans-serif;
    }

    .table {
        width: 100%;
    }
    .table thead th {
        padding: 10px 10px;
        background: #00adee;
        font-size: 15px;
        text-transform: uppercase;
        vertical-align: top;
        color: #1D4A5A;
        font-weight: normal;
        text-align: left;
    }
    .table tbody tr td {
        padding: 10px;
        background: #f2f2f2;
        font-size: 14px;
        line-height: 1.8rem;
    }

    .add, .edit, .save, .delete ,.cancel{
        outline: none;
        background: none;
        border: none;
    }

    .edit, .save, .delete ,.cancel{
        padding: 5px 10px;
        cursor: pointer;
    }

    .add {
        float: right;
        background: transparent;
        border: 1px solid #ffffff;
        color: #ffffff;
        font-size: 13px;
        padding: 0;
        padding: 3px 5px;
        cursor: pointer;
    }
    .add:hover {
        background: #ffffff;
        color: #00adee;
    }
    .loading{
         display: none;
    }
    .save {
        display: none;
        background: #32AD60;
        color: #ffffff;
    }
    .save:hover {
        background: #27854a;
    }

    .edit {
        background: #2199e8;
        color: #ffffff;
    }
    .edit:hover {
        background: #147dc2;
    }

    .delete ,.cancel{
        background: #EC5840;
        color: #ffffff;
    }
    .delete:hover ,.cancel{
        background: #e23317;
        
    }
    .cancel{
         display: none;
    }
body input[type="text"] {
  padding-left: 0.5rem;
  margin-left: -0.5rem;
  width: 100%;
  line-height: 1.8rem;
  font-size: 1rem;
  font-family: inherit;
  background-color: #f5f5f5;
  border: 0px;
  
}

</style>
<script src="jquery-3.3.1.js" type="text/javascript"></script>
<script src="ajax.js" type="text/javascript"></script>
<script src="jquery-3.3.1.min.js" type="text/javascript"></script>
<script>
    var v,b,c;
    var a=0;
    $(document).on('click', '.edit', function () {
        $(this).parent().siblings('td.data').each(function () {
            var content = $(this).html();
            a++;
            if(a==1){
                v=content;
            }
            if(a==2){
                  b=content;
            }
            if(a==3){
                  c=content;
            }
            $(this).html('<input type="text" value="' + content + '" />');
        });

        $(this).siblings('.save').show();
        $(this).siblings('.cancel').show();
        $(this).siblings('.delete').hide();
        $(this).hide();
    });

    $(document).on('click', '.save', function () {

var id="";
var name="";
var desc="";
var dem=0;
  $('input').each(function () {
      dem++;
       var content = $(this).val();
          
      if(dem==1){
         
           id=content;
      }if(dem==2){
           name=content;
      }
      if(dem==3){
           desc=content;
      }
           
        });

      
  $.ajax({
       
            type: "GET",
            data: {name: name,id: id, desc: desc},
            url: "Edit",
            success: function (responseJson) {
                $.each(responseJson, function (key, value) {
                  
if(value=="true"){
            $('input').each(function () {
            var content = $(this).val();
            $(this).html(content);
            $(this).contents().unwrap();
        });
       $('.save').hide();
              $('.edit').show();
              $('.cancel').hide();
                $('.delete').show();
}else{
      alert("lỗi");
       
         
     
}
                });
            }
        })

  

    });
$(document).on('click', '.cancel', function () {
    $('.save').hide();
              $('.edit').show();
              $('.cancel').hide();
                $('.delete').show();
        var d=0;
      $('input').each(function () {
            var content = $(this).val();
            d++;
            if(d==1){
                $(this).html(v);
            $(this).contents().unwrap();
                
            }
            
             if(d==2){
                $(this).html(b);
            $(this).contents().unwrap();
                
            }
            
             if(d==3){
                $(this).html(c);
            $(this).contents().unwrap();
                
            }
            
            
        });
    });




 $(function(){
        $('#table .delete').click(function(e) {
            e.preventDefault();
            var a=$(this).closest('tr').find('td:first').text();
           
              $.ajax({
            type: "GET",
            data: {name: "delete",id: a},
            url: "Edit",
            success: function (responseJson) {
                $.each(responseJson, function (key, value) {
                  
if(value=="true"){
          $(this).parents('tr').remove();
}else{
      alert("lỗi");
       
         
     
}
                });
            }
        })
        });
    });

    $('.add').click(function () {
        $(this).parents('table').append('<tr><td class="data"></td><td class="data"></td><td class="data"></td><td><button class="save">Save</button><button class="edit">Edit</button> <button class="delete">Delete</button></td></tr>');
    });
</script>
